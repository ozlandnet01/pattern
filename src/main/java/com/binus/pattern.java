package com.binus;

public class pattern {
    public static void main(String[] args) {
        int NUM = 5;
        for (int i = 0 ; i < NUM ; i++){
            for(int j = 0 ; j < i ; j++){
                System.out.print("*");
                if(j == i-1) System.out.println();
            }
        }

        System.out.println();

        for (int i = 0 ; i < NUM ; i++){
            for(int j = 0 ; j < NUM ; j++){
                if(j > i)
                    System.out.print("*");
                else System.out.print(" ");
                if(j == NUM-1) System.out.println();
            }
        }
    }
}
